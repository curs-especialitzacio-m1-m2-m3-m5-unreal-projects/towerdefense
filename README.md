# Tower Defense Project Information
## Author
Nohadon (Pol Gonzalo Méndez)
## Introduction
This project is the Activity 2 of the Unreal Module.

## Risketos
### Basics
- [X] Capacitat d’elecció, per exemple diferents torretes.
- [X] El HUD es crea i s’emplena per BP.
- [X] Ús d’esdeveniments i dispatchers, tot ha d’estar desacoplat.
- [X] Enemics o objectius a on atacar i en fer-ho que morin.
- [ ] Ús d’un actor component o Interfície.
### Adicionals
- [X] Sistema de Waves
- [X] Escalabilitat de dificultat
- [X] Escalabilitat per a creació de torretes 
